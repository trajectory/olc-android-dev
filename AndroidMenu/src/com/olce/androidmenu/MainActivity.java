package com.olce.androidmenu;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_new:
			// Action ketika item action_new di klik
			Toast.makeText(this, "Option menu new pressed", Toast.LENGTH_LONG).show();
			break;
		case R.id.action_two:
			// Action ketika item action_two di klik
			Toast.makeText(this, "Option menu two pressed", Toast.LENGTH_LONG).show();
			break;
		case R.id.action_three:
			// Action ketika item action_three di klik
			Toast.makeText(this, "Option menu three pressed", Toast.LENGTH_LONG).show();
			break;
		case R.id.action_settings:
			// Action ketika item action_settings di klik
			Toast.makeText(this, "Option menu setting pressed", Toast.LENGTH_LONG).show();
			break;		
		}
		return super.onOptionsItemSelected(item);
	}

}
