package com.olc.namaplanet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MySimpleArrayAdapter extends ArrayAdapter<String> {
	private final Context context;
	private final String[] values;

	public MySimpleArrayAdapter(Context context, String[] values) {
		super(context, R.layout.row_layout, values);
		this.context = context;
		this.values = values;
	}
	public View getView(int position, View convertView, ViewGroup parent){
		// Inflate rowView
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.row_layout, parent, false);
		
		// Deklarasi TextView dan ImageView untuk menampilkan Teks dan Icon
		TextView textView = (TextView) rowView.findViewById(R.id.text1);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
		
		// Menampilkan nama planet pada textView, sesuai dengan posisinya
		textView.setText(values[position]);
		
		// Menampilkan Icon yang berbeda untuk Earth, dan ditampilkan pada imageView
		if(values[position].startsWith("Earth")){
			// Jika Earth maka iconnya berbeda
			imageView.setImageResource(android.R.drawable.star_big_on);
		}else{
			// Jika bukan Earth
			imageView.setImageResource(android.R.drawable.star_big_off);
		}
		
		return rowView;
	}
	
}
