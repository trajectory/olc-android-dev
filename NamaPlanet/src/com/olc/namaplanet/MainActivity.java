package com.olc.namaplanet;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity  {

	// Deklarasi nama-nama planet yang akan ditampilkan
	private String[] planets = new String[] { "Mercury", "Venus", "Earth", "Mars",
			"Jupiter", "Saturn", "Uranus", "Neptune" };
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// Mengambil ListView dari layout/activity_main.xml
		ListView listku = (ListView) findViewById(R.id.daftarku);
		
		
		// Deklarasi adapter, adapter digunakan sebagai penghubung antara ListView dengan Object yang akan ditampilkan
		// Pada kasus ini Objectnya adalah Array of String
		/* Kode berikut tidak digunakan karena sudah menggunakan custom adapter MySimpleArrayAdapter
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				getApplicationContext(), R.layout.row_layout,
				R.id.text1, planets);*/
		
		// Menggunakan adaptor custom
		MySimpleArrayAdapter adapter = new MySimpleArrayAdapter(getApplicationContext(), planets);
		
		// Memasangkan adapter pada listku
		listku.setAdapter(adapter);
		
		// Deklarasi OnItemClickListener Callback
		OnItemClickListener itemClickListener = new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int position,
					long id) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), planets[position], Toast.LENGTH_LONG).show();
			}
			
		};
		
		// Registrasi callback
		listku.setOnItemClickListener(itemClickListener);
	}
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
