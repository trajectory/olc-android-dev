package com.olc.simplenote;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.olc.simplenote.model.Note;

public class DBAdapter {

	private DBHelper dbHelper;
	private SQLiteDatabase db;

	// Semua kolom yang ada pada table note, digunakan untuk query
	public static final String[] NOTE_ALL_COLOUMNS = new String[] {
			DBHelper.NOTE_ID_COLOUMN, DBHelper.NOTE_TITLE_COLOUMN,
			DBHelper.NOTE_CONTENT_COLOUMN };

	public DBAdapter(Context context) {
		dbHelper = new DBHelper(context);
		db = null;
	}

	// Membuka koneksi database
	public void open() {
		db = dbHelper.getWritableDatabase();
	}

	// menutup koneksi database
	public void close() {
		db.close();
	}

	// Query seluruh note yang ada pada database
	public ArrayList<Note> getAllNotes() {
		ArrayList<Note> notes = new ArrayList<Note>();
		// Query tanpa argumen where
		// Hasil suatu query merupakan Cursor yang menyimpan data pasangan
		// antara key-value dalam beberapa baris
		Cursor cursor = db.query(DBHelper.TABLE_NOTE, NOTE_ALL_COLOUMNS, null,
				null, null, null, null);
		// Sebelum mengambil data pastikan Cursor memiliki data, dengan method
		// moveToFirst()
		if (cursor.moveToFirst()) {
			// Mengambil data kemudian membuat note baru dan kemudian
			// ditambahkan pada notes secara berulang
			do {
				long id = cursor.getLong(0);
				String title = cursor.getString(1);
				String content = cursor.getString(2);
				Note note = new Note(id, title, content);
				notes.add(note);
				// Hingga cursor tidak memiliki baris lagi
			} while (cursor.moveToNext());
		}
		return notes;
	}

	// Query suatu note berdasarkan id
	public Note getNote(long id) {
		Note note = null;
		// query(table, columns, selection, selectionArgs, groupBy, having,
		// orderBy)
		// Query dengan suatu argumen, tidak langsung semua diletakan pada
		// parameter selection(where)
		// tetapi menggunakan simbol ?
		// dan parameternya digunakan pada selectionArgs dalam bentuk array of
		// string
		Cursor cursor = db.query(DBHelper.TABLE_NOTE, NOTE_ALL_COLOUMNS,
				DBHelper.NOTE_ID_COLOUMN + "=?",
				new String[] { Long.toString(id) }, null, null, null);

		if (cursor.moveToFirst()) {
			note = new Note(cursor.getLong(0), cursor.getString(1),
					cursor.getString(2));
		}
		return note;
	}

	// Delete note berdasarkan id
	public long deleteNote(long id) {
		long result = db.delete(DBHelper.TABLE_NOTE, DBHelper.NOTE_ID_COLOUMN
				+ "=?", new String[] { Long.toString(id) });
		return result;
	}

	// Update note yang sudah ada
	private long update(Note note) {
		// Dibutuhkan suatu ContentValues untuk menyimpan pasangan Key(nama
		// kolom) dengan data yang akan disimpan
		ContentValues contentValues = new ContentValues();
		contentValues.put(DBHelper.NOTE_ID_COLOUMN, note.getId());
		contentValues.put(DBHelper.NOTE_TITLE_COLOUMN, note.getTitle());
		contentValues.put(DBHelper.NOTE_CONTENT_COLOUMN, note.getContent());
		long result = db.update(DBHelper.TABLE_NOTE, contentValues,
				DBHelper.NOTE_ID_COLOUMN + "=?",
				new String[] { Long.toString(note.getId()) });
		return result;
	}

	// Menambahkan note ke database
	private long add(Note note) {
		ContentValues contentValues = new ContentValues();
		contentValues.put(DBHelper.NOTE_TITLE_COLOUMN, note.getTitle());
		contentValues.put(DBHelper.NOTE_CONTENT_COLOUMN, note.getContent());
		long result = db.insert(DBHelper.TABLE_NOTE, null, contentValues);
		return result;
	}

	// Save note, baik baru maupun yang sudah ada
	// Jika note baru maka menggunakan add
	// jika note sudah pernah disimpan sebelumnya maka gunakan update
	public long save(Note note) {
		if (note.getId() == Note.NEW_NOTE_ID) {
			return add(note);
		} else {
			return update(note);
		}
	}
}
