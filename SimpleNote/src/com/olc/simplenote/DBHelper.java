package com.olc.simplenote;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {

	// Database name
	public static final String DATABASE_NAME = "db_note";

	// Table name
	public static final String TABLE_NOTE = "note";

	// Table name coloumn
	public static final String NOTE_ID_COLOUMN = "_id";
	public static final String NOTE_TITLE_COLOUMN = "title";
	public static final String NOTE_CONTENT_COLOUMN = "content";

	// Create table note
	public static final String CREATE_TABLE_NOTE = "CREATE TABLE " + TABLE_NOTE
			+ "(" + NOTE_ID_COLOUMN + " INTEGER PRIMARY KEY AUTOINCREMENT,"
			+ NOTE_TITLE_COLOUMN + " TEXT, " + NOTE_CONTENT_COLOUMN
			+ " TEXT NOT NULL)";

	// Database version, important for upgrade
	public static final int DATABASE_VERSION = 1;

	// Constructor
	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Create table for the first time
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_NOTE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Upgrade jika versi baru tersedia
		if (newVersion > oldVersion) {
			Log.w(DBHelper.class.getName(), "Upgrading database from version "
					+ oldVersion + " to " + newVersion
					+ ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTE);
			db.execSQL(CREATE_TABLE_NOTE);
		}
	}

	// +------+--------+----------+
	// | _id | title | content |
	// +------+--------+----------+
}
