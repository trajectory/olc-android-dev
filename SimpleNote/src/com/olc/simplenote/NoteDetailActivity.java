package com.olc.simplenote;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.olc.simplenote.model.Note;

public class NoteDetailActivity extends Activity {

	public static final String ID_KEY = "id";

	private EditText titleForm, contentForm;

	private Note note = null;

	private DBAdapter dbAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_note_detail);

		dbAdapter = new DBAdapter(getApplicationContext());

		titleForm = (EditText) findViewById(R.id.titleForm);
		contentForm = (EditText) findViewById(R.id.contentForm);

		long id = getIntent().getExtras().getLong(ID_KEY);
		// Load note ketika Activity dijalankan
		load(id);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.note_detail_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Save action
		case R.id.action_save:
			if (save()) {
				finish();
				Toast.makeText(getApplicationContext(), R.string.save_success,
						Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(getApplicationContext(), R.string.save_fail,
						Toast.LENGTH_SHORT).show();
			}
			break;
			// Delete action
		case R.id.action_delete:
			if (deleteNote()) {
				finish();
				Toast.makeText(getApplicationContext(), R.string.delete_success,
						Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(getApplicationContext(), R.string.delete_fail,
						Toast.LENGTH_SHORT).show();
			}
			break;
			// cancel action
		case R.id.action_cancel:
			finish();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private boolean deleteNote() {
		dbAdapter.open();
		long result = dbAdapter.deleteNote(note.getId());
		dbAdapter.close();
		if (result > 0) {
			return true;
		} else {
			return false;
		}
	}

	private boolean save() {
		updateNote();
		dbAdapter.open();
		long result = dbAdapter.save(note);
		dbAdapter.close();
		if (result > 0) {
			return true;
		} else {
			return false;
		}
	}

	private void load(long id) {
		note = getNote(id);
		titleForm.setText(note.getTitle());
		contentForm.setText(note.getContent());
	}

	private Note getNote(long id) {
		Note note = null;
		if (id == Note.NEW_NOTE_ID) {
			note = new Note("", "");
		} else {
			dbAdapter.open();
			note = dbAdapter.getNote(id);
			dbAdapter.close();
		}
		return note;
	}

	private void updateNote() {
		note.setTitle(titleForm.getEditableText().toString());
		note.setContent(contentForm.getEditableText().toString());
	}
}
