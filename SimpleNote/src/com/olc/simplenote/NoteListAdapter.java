package com.olc.simplenote;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.olc.simplenote.model.Note;

public class NoteListAdapter extends ArrayAdapter<Note> {

	private ArrayList<Note> notes;
	private Context context;

	public NoteListAdapter(Context context, ArrayList<Note> notes) {
		super(context, R.layout.row_layout, R.id.titleView, notes);
		// TODO Auto-generated constructor stub
		this.notes = notes;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.row_layout, parent, false);
		TextView titleView = (TextView) rowView.findViewById(R.id.titleView);
		TextView contentView = (TextView) rowView
				.findViewById(R.id.contentView);

		Note note = notes.get(position);

		titleView.setText(note.getTitle());
		contentView.setText(note.getContent());

		return rowView;
	}
}
