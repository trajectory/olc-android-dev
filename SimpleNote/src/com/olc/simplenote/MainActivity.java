package com.olc.simplenote;

import java.util.ArrayList;

import com.olc.simplenote.model.Note;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class MainActivity extends Activity {

	private ListView mListView;
	private DBAdapter dbAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		dbAdapter = new DBAdapter(getApplicationContext());
		
		mListView = (ListView) findViewById(R.id.noteList);
		// Mengupdate listview
		loadNote();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_add:
			Intent intent = new Intent();
			intent.setClass(getApplicationContext(), NoteDetailActivity.class);
			intent.putExtra(NoteDetailActivity.ID_KEY, Note.NEW_NOTE_ID);
			startActivity(intent);
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	// ListView onItemClickListener digunakan untuk menghandle ketika item pada ListView diklik
	// Ketika diklik maka akan membuka editor(Activity NoteDetailActivity) berdasarkan id note yang dipilih
	private AdapterView.OnItemClickListener mOnItemClickListener = new AdapterView.OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> adapterView, View view, int position,
				long id) {
			Intent intent = new Intent();
			intent.setClass(getApplicationContext(), NoteDetailActivity.class);
			Note selectedNote = (Note) mListView.getAdapter().getItem(position);
			intent.putExtra(NoteDetailActivity.ID_KEY, selectedNote.getId());
			startActivity(intent);
		}
	};

	private void loadNote() {
		dbAdapter.open();
		ArrayList<Note> notes = dbAdapter.getAllNotes();
		dbAdapter.close();
		NoteListAdapter listAdapter = new NoteListAdapter(getApplicationContext(), notes);
		mListView.setAdapter(listAdapter);
		mListView.setOnItemClickListener(mOnItemClickListener);
	}

	// Ketika activity dijlankan kembali maka update User interface
	// Untuk menampilkan data baru yang baru ditambahkan
	public void onResume(){
		super.onResume();
		loadNote();
	}
}
