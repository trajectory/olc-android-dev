package com.olc.simplenote.model;

public class Note {
	private long id;
	private String title, content;
	
	public static final long NEW_NOTE_ID = Long.MIN_VALUE;

	public Note(long id, String title, String content) {
		// Constructor
		setId(id);
		setTitle(title);
		setContent(content);
	}
	
	public Note(String title, String content) {
		// Constructor
		setId(NEW_NOTE_ID);
		setTitle(title);
		setContent(content);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
