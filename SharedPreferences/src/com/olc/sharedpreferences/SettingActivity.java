package com.olc.sharedpreferences;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class SettingActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);
		SharedPreferences sp = getSharedPreferences(MainActivity.PREF_NAME, Context.MODE_PRIVATE);
		
		// Nama
		String name = sp.getString(MainActivity.NAME_KEY, "");
		// Nomor telepon
		String phone = sp.getString(MainActivity.PHONE_KEY, "");
		// Alamat
		String address = sp.getString(MainActivity.ADDRESS_KEY, "");
		
		EditText nameForm = (EditText) findViewById(R.id.nama_form);
		EditText phoneForm = (EditText) findViewById(R.id.nomor_telepon_form);
		EditText addressForm = (EditText) findViewById(R.id.alamat_form);
		
		nameForm.setText(name);
		phoneForm.setText(phone);
		addressForm.setText(address);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.setting, menu);
		return true;
	}

	// Button Save handler
	public void save(View v){
		EditText nameForm = (EditText) findViewById(R.id.nama_form);
		EditText phoneForm = (EditText) findViewById(R.id.nomor_telepon_form);
		EditText addressForm = (EditText) findViewById(R.id.alamat_form);
		String name = nameForm.getEditableText().toString();
		String phone = phoneForm.getEditableText().toString();
		String address = addressForm.getEditableText().toString();
		
		SharedPreferences sp = getSharedPreferences(MainActivity.PREF_NAME, Context.MODE_PRIVATE);
		Editor edit = sp.edit();
		// Save name
		edit.putString(MainActivity.NAME_KEY,name );
		// Save phone
		edit.putString(MainActivity.PHONE_KEY, phone);
		// Save address
		edit.putString(MainActivity.ADDRESS_KEY, address);
		// Commit
		boolean commitStatus = edit.commit();
		// Show result to user
		if(commitStatus){
			Toast.makeText(getApplicationContext(), "Success!", Toast.LENGTH_LONG).show();
		}else{
			Toast.makeText(getApplicationContext(), "Failed to save!", Toast.LENGTH_LONG).show();
		}
	}
	
	
	
	// Button Cancel handler
	public void cancel(View v){
		// Finish activty agar kembali ke MainActivity
		finish();
	}
}
