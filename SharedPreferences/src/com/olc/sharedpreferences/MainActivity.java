package com.olc.sharedpreferences;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {

	// Agar memudahkan pengambilan SharedPreference, maka namanya bisa
	// dideklarasikan secara konstan, statis dan global
	public static final String PREF_NAME = "DataDiriPreference";

	// Key pengambilan data, tujuannya sama
	public static final String NAME_KEY = "name";
	public static final String PHONE_KEY = "phone";
	public static final String ADDRESS_KEY = "address";
	public static final String COUNT_KEY = "count";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Mengambil shared preferences
		// Mengambil shared preferences ini sama dengan pada onResume() sehingga
		// bisa saja dijadikan satu method agar penggunaanya mudah
		SharedPreferences sp = getSharedPreferences(PREF_NAME,
				Context.MODE_PRIVATE);

		// Nama
		String name = sp.getString(NAME_KEY, "");
		// Nomor telepon
		String phone = sp.getString(PHONE_KEY, "");
		// Alamat
		String address = sp.getString(ADDRESS_KEY, "");
		// Counter
		int count = sp.getInt(COUNT_KEY, 0);

		TextView nameText = (TextView) findViewById(R.id.nama_text);
		TextView phoneText = (TextView) findViewById(R.id.nomor_telepon_text);
		TextView addressText = (TextView) findViewById(R.id.alamat_text);
		TextView countText = (TextView) findViewById(R.id.conuter_text);

		nameText.setText(name);
		phoneText.setText(phone);
		addressText.setText(address);

		if (count == 1) {
			// Menampilkan welcome message jika baru pertama kali membuka
			countText.setText(R.string.welcome);
		} else {
			countText.setText("Anda sudah membuka aplikasi ini sebanyak "
					+ count + " kali.");
		}
		// Menambahkan counter
		count++;
		Editor edit = sp.edit();
		edit.putInt(COUNT_KEY, count);
		edit.commit();
	}

	// onResume() dipanggil ketika activty dijalankan ulang
	// Ini digunakan agar Activity memuat ulang data ketika selesai dari
	// SettingActivity
	public void onResume() {
		super.onResume();
		// Mengambil shared preferences
		// Mengambil shared preferences ini sama dengan pada onCreate() sehingga
		// bisa saja dijadikan satu method agar penggunaanya mudah
		SharedPreferences sp = getSharedPreferences(PREF_NAME,
				Context.MODE_PRIVATE);

		// Nama
		String name = sp.getString(NAME_KEY, "");
		// Nomor telepon
		String phone = sp.getString(PHONE_KEY, "");
		// Alamat
		String address = sp.getString(ADDRESS_KEY, "");
		// Counter
		int count = sp.getInt(COUNT_KEY, 0);

		TextView nameText = (TextView) findViewById(R.id.nama_text);
		TextView phoneText = (TextView) findViewById(R.id.nomor_telepon_text);
		TextView addressText = (TextView) findViewById(R.id.alamat_text);
		TextView countText = (TextView) findViewById(R.id.conuter_text);

		nameText.setText(name);
		phoneText.setText(phone);
		addressText.setText(address);

		if (count == 1) {
			// Menampilkan welcome message jika baru pertama kali membuka
			countText.setText(R.string.welcome);
		} else {
			countText.setText("Anda sudah membuka aplikasi ini sebanyak "
					+ count + " kali.");
		}

	}

	// Ketika menu Setting dipilih
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			Intent i = new Intent();
			i.setClass(getApplicationContext(), SettingActivity.class);
			startActivity(i);
			break;
		default:
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	// Reset Counter
	public void resetCounter(View v) {
		int count = 1;
		TextView countText = (TextView) findViewById(R.id.conuter_text);
		SharedPreferences sp = getSharedPreferences(PREF_NAME,
				Context.MODE_PRIVATE);
		Editor edit = sp.edit();
		edit.putInt(COUNT_KEY, count);
		edit.commit();

		// Menampilkan welcome message jika baru pertama kali membuka
		countText.setText(R.string.welcome);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
