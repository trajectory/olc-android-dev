package com.olc.radiobutton;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends Activity {
	private RadioGroup group;
	private RadioButton sby, eys, mad, smb;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		group = (RadioGroup) findViewById(R.id.group);

		sby = (RadioButton) findViewById(R.id.sby);
		eys = (RadioButton) findViewById(R.id.eys);
		mad = (RadioButton) findViewById(R.id.mad);
		smb = (RadioButton) findViewById(R.id.smb);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void jawab(View view) {
		int selectedID = group.getCheckedRadioButtonId();
		RadioButton checked = (RadioButton) findViewById(selectedID);
		
		if(checked == sby){
			Toast.makeText(getApplicationContext(), "Selamat, anda benar.", Toast.LENGTH_LONG).show();
		}else{
			Toast.makeText(getApplicationContext(), "Ada yang salah dengan anda.", Toast.LENGTH_LONG).show();
		}
	}

}
