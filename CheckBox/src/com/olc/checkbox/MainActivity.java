package com.olc.checkbox;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

public class MainActivity extends Activity {

	private CheckBox hitam, biru, ungu, orange, hijau;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		hitam  = (CheckBox) findViewById(R.id.hitam);
		biru   = (CheckBox) findViewById(R.id.biru);
		ungu   = (CheckBox) findViewById(R.id.ungu);
		orange = (CheckBox) findViewById(R.id.orange);
		hijau  = (CheckBox) findViewById(R.id.hijau);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void okay(View view){
		String output = "Warna yang anda sukai :";
		if (hitam.isChecked()) {
			output += "\nHitam";
		}
		if (ungu.isChecked()) {
			output += "\nUngu";
		}
		if (biru.isChecked()) {
			output += "\nBiru";
		}
		if (orange.isChecked()) {
			output += "\nOrange";
		}
		if (hijau.isChecked()) {
			output += "\nHijau";
		}

		Toast.makeText(getApplicationContext(), output, Toast.LENGTH_SHORT)
				.show();
	}

}
