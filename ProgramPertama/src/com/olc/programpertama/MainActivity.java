package com.olc.programpertama;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	// Dijalankan ketika Activity di jalankan
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    public void openActivity(View view){
    	Toast.makeText(this, "Membuka Activity Ke-2", Toast.LENGTH_LONG).show();    	
    	EditText formNama = (EditText) findViewById(R.id.form_nama);
    	String nama = formNama.getEditableText().toString();
    	Intent i = new Intent();
    	i.setClass(MainActivity.this, ActivityKeDua.class);
    	i.putExtra("nama", nama);
    	startActivity(i); 	
    }
}
