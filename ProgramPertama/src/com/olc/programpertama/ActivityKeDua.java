package com.olc.programpertama;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.TextView;

public class ActivityKeDua extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_activity_ke_dua);
		
		// Deklarasi textview
		TextView textViewName = (TextView) findViewById(R.id.nama);
		
		// Mengambil nilai dari Activity sebelumnya(MainActivity)
		String nama = getIntent().getExtras().getString("nama");
		
		textViewName.setText(nama);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_ke_dua, menu);
		return true;
	}

}
