package com.olc.layoutexample;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class LinearVerticalActivty extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_activity_linear_vertical);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_linear_vertical, menu);
		return true;
	}

}
