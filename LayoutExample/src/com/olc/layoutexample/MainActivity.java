package com.olc.layoutexample;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends Activity {

	// Activity ini berisi list yang dapat digunakan untuk memilih contoh layout
	
	// Jenis - jenis layout yang ada pada contoh
	public static final String[] layouts = new String[] {
			"Linear (Horizontal)", "Linear (Vertical)", "Relative", "Absolute",
			"Table Layout", "Kombinasi" };
	
	private ListView list;
	private ArrayAdapter<String> listAdapter;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		list = (ListView) findViewById(R.id.listView);
		listAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.row_layout, R.id.text1, layouts);
		//Registrasi adapter pada ListView
		list.setAdapter(listAdapter);
		
		// Registrasi OnItemClickListener callback
		list.setOnItemClickListener(listItemClickListener);
		
	}
	
	// Callback ketika item pada listview di click
	private OnItemClickListener listItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position,
				long id) {
			// Posisi pada ListView
			// 0. Linear (Horizontal)
			// 1. Linear (Vertical)
			// 2. Relative
			// 3. Absolute
			// 4. Table Layout
			// 5. Kombinasi
			
			// Mempersiapkan intent
			Intent intent = new Intent();
			
			switch (position) {
			case 0:
				// Linear Horizontal dipilih
				intent.setClass(getApplicationContext(), LinearHorizontalActivity.class);
				break;
			case 1:
				// Linear Vertical dipilih
				intent.setClass(getApplicationContext(), LinearVerticalActivty.class);
				break;
			case 2:
				// Relative dipilih
				intent.setClass(getApplicationContext(), RelativeLayoutActivty.class);
				break;
			case 3:
				// Absolute dipilih
				// Sangat tidak dianjurkan menggunakan RelativeLayout
				// Karena dengan meggunakan RelativeLayout maka aplikasi kita tidak dapat digunakan pada berbagai macam ukuran layar
				// Selain itu RelativeLayout sudah dinyatakan deprecated (sudah basi)
				intent.setClass(getApplicationContext(), AbsoluteLayoutActivty.class);
				break;
			case 4:
				// Table dipilih 
				intent.setClass(getApplicationContext(), TableLayoutActivty.class);
				break;
			case 5:
				// Kombinasi dipilih
				// Kita dapat menggabungkan berbagai macam jenis layout pada 1 file layout seperti contoh berikut
				intent.setClass(getApplicationContext(), KombinasiLayoutActivty.class);
				break;
			}
			
			// Memulai activity baru
			startActivity(intent);
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
